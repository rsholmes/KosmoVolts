# KosmoVoltsMain.kicad_sch BOM

Tue 31 Jan 2023 04:00:49 PM EST

Generated from schematic by Eeschema 7.0.0-rc2-unknown-50e9685490~164~ubuntu22.04.1

**Component Count:** 22

| Refs | Qty | Component | Description | Vendor | SKU |
| ----- | --- | ---- | ----------- | ---- | ---- |
| C1, C2 | 2 | 100nF | Ceramic capacitor, 2.5 mm pitch | Tayda |  |
| GATES1, GATES2 | 2 | MA08-2 | Generic connector, double row, 02x08, odd/even pin numbering scheme (row 1 odd numbers, row 2 even numbers), script generated (kicad-library-utils/schlib/autogen/connector/) |  |  |
| J1 | 1 | 2_pin_Molex_connector | KK254 Molex connector | Tayda | A-826 |
| J2 | 1 | OUT | Audio Jack, 2 Poles (Mono / TS) | Tayda | A-1121 |
| J3 | 1 | OUT | KK254 Molex header | Tayda | A-804 |
| R1, R2, R3, R4, R5, R6, R7, R8 | 8 | 10K | Resistor | Tayda |  |
| R9 | 1 | 1K | Resistor | Tayda |  |
| RV1, RV2, RV3, RV4, RV5 | 5 | B50K | Potentiometer | Tayda |  |
| U1 | 1 | TL072 | Dual operational amplifier, DIP-8 | Tayda | A-037 |
    
