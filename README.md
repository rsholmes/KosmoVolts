# HP Potentials

Music Thing Modular [Volts Expander](https://github.com/TomWhitwell/Volts) converted to Kosmo format "KosmoVolts" [design](https://github.com/BenRufenacht/KosmoVolts) by Ben Rufenacht, renamed to "HP Potentials" and panel design revised by Rich Holmes. 

[Original schematic](https://github.com/TomWhitwell/Volts/blob/master/Collateral/Volts_Schematic.pdf)

License:  
[CC-BY-SA](https://creativecommons.org/licenses/by-sa/3.0/) 

# Design changes

My changes to Rufenacht's version are entirely stylistic:

* Panel converted to AO style
* Changed to slotted hole footprint for jack
* Changed PCB and panel labels

## Current draw
5 mA +12 V, 4 mA -12 V

## Photos

![kosmovolts](Images/kosmovolts.jpg)

## Documentation

* [My schematic](Docs/KosmoVoltsMain.pdf)
* PCB layout: [front](Docs/KosmoVoltsMain_layout_front.pdf), [back](Docs/KosmoVoltsMain_layout_back.pdf)
* [BOM](Docs/KosmoVoltsMain_bom.md)

## Git repository

* [https://gitlab.com/rsholmes/KosmoVolts](https://gitlab.com/rsholmes/KosmoVolts)


